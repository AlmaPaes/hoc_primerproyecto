\documentclass{article}
\textheight=19cm
\textwidth=14cm
\topmargin=-2cm
\oddsidemargin = 1cm
\usepackage{amsmath,amssymb,amsfonts,latexsym,textcomp}
\usepackage[table]{xcolor} %agregar color a la tabla
\usepackage{graphicx,caption,subcaption}
%\graphicspath{ {./Imagenes/} }
\usepackage{float}
\restylefloat{table}
%fecha en español
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[left = 2.5cm, bottom = 2cm, top=2cm]{geometry}
\usepackage{tcolorbox}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=cyan,    
    urlcolor=magenta,
}

\definecolor{lightmauve}{rgb}{0.86, 0.82, 1.0}
\definecolor{coralpink}{rgb}{0.97, 0.51, 0.47}
\definecolor{lightcarminepink}{rgb}{0.9, 0.4, 0.38}
\definecolor{airforceblue}{rgb}{0.36, 0.54, 0.66}

\usepackage{fancyhdr}
 
\pagestyle{fancy}
\fancyhf{}
\rhead{Páes Alcalá Alma Rosa}
\lhead{Primer Proyecto: Traveling Saleman Problem}


\begin{document}
\title{
  {\sc Universidad Nacional Autónoma de México}\\
  {\sc Facultad de Ciencias}\\
  {\sc Seminario de Heurísticas y Optimización Combinatoria 2021-1}\\
  {\sc Reporte de Proyecto 1: Traveling Salesman Problem}\\
}
\author{Páes Alcalá Alma Rosa}
%fecha
\date{20 de noviembre de 2020}
\maketitle
 
\section*{Introducción}

\noindent
El problema del agente viajero, o en su idioma original, \emph{Travelling Salesman Problem}, es un conocido problema de optimización que consiste en encontrar el tour de longitud más corta dentro de una gráfica.\\

Se considera en un principio a las gráficas no dirigidas, aunque puede adaptarse a gráficas dirigidas; donde cara arista tiene un peso. Es decir, sea $G=(V,E)$ nuestra gráfica, existe una función $\omega$ tal que:

\[
\omega: E \longrightarrow \mathbb{R}^+, \; \omega((i,j)) = w
\]

Por otro lado, aunque el téŕmino de \textbf{tour} no tiene un consenso universal entre muchas de las fuentes confiables entre la teoría de gráficas, y algunas ni siquiera lo mencionan; se considera que un tour es una secuencia de aristas, que coincide con una secuena de vértices, cuyo vértice inicial y final son el mismo, donde ninguna arista se repite más de una vez.\\

La manera más común de resolver este problema es a través de una metodología llamada \emph{mejoramiento iterativo}, que comienza con una configuración inicial a la solución del problema, la reorganiza buscando una mejor solución, y si la encuentra, guarda esta nueva solución; y el proceso termina cuando ya no pueden ser encontradas mejores soluciones. Para la aplicación de esta solución, es necesario contar con una representación concisa de las soluciones, tanto inicial como las consecuentes; una función que nos permita representar con un sólo número la calidad de las soluciones encontradas, siendo ésta la función que se busca optimizar; y un procedimiento para encontrar soluciones mejores. Así mismo, como con cualquier problema de optimización, necesitamos una manera de saber si nuestras soluciones pueden ser viables para el problema que buscamos resolver.\\

El recocido simulado -o \emph{simulated annealing}- es una optimización al mejoramiento iterativo, basado en el apagado de salpicaduras -o \emph{splat quenching}-, que consiste en derretir metales a una temperatura suficientemente alta; y proceder al enfriado rápido del material, buscando un equilibrio de la temperatura, y obteniendo un material \emph{cristalizado}, o con cierta granularidad. \\

Abstrayendo este proceso, y llevándolo a la resolución de problemas, necesitaríamos inicialmente elegir una configuración y temperatura inicial, y en cada paso del algoritmo se realizarían cambios a la vieja configuración para buscar más soluciones. Si una de ellas llega a ser mejor que la vieja, se actualiza como la configuración a superar; y si es peor, sólo se aceptará con cierta probabilidad para el siguiente paso. Al tiempo que esto sucede, la temperatura se va decrementando. Este algoritmo se detiene cuando ya no hay mejores soluciones.\\

La aceptación por umbrales -o \emph{threshold accepting}- es un método similar al recocido simulado, que toma como configuración inicial una permutación aleatoria de los vértices, y obtiene una nueva configuración haciendo pequeños cambios al tour. Difiere también del recocido simulado en su forma de determinar si una nueva solución puede ser tomada como la configuración que procederemos a modificar. La aceptación por umbrales acepta soluciones nuevas sólo si no son mucho peores que la actual, evitando caer así en un mínimo local. \\

Aterrizando todo esto en el proyecto que presenta este reporte, se buscó encontrar la mejor ruta para recorrer una cantidad no despreciable de ciudades, omitiendo la restricción de que debe empezar y acabar en la misma ciudad. Para resolver este problema, se utilizó en esencia el método de aceptación por umbrales, apoyándose de un algoritmo auxiliar para encontrar la temperatura inicial adecuada para enfriar; o, aplicado al procedimiento metalúrgico, la temperatura de fusión.


La descripción de la implementación y los resultados están escritos para la versión actualizada del 13 de noviembre de 2020, cuyo código fuente está en \cite{Repository}. Se utilizó como guía principal el texto proporcionado por el Dr. Canek Pelaez\cite{Canekbook}; el cual presenta a detalle -el necesario- los algoritmos necesarios para resolver este problema. 



\section*{Implementación}

\noindent
En la búsqueda del lenguaje de programación para implementar esta heurística, recolecté una lista de los lenguajes que me interesara aprender, y que de hecho fuera buena idea manejar con él un proyecto de esta magnitud; y con lista en mano, pedí opinión a varias de mis más íntimas amistades. Con esto, llegué a la conclusión de que \textbf{Rust} sería una buena idea. La elección del manejador de proyectos \textbf{Cargo} vino de la mano con el lenguaje de programación, ya que es su manejador por default; y ambos están bellamente documentados.\\

Dejando de lado las funciones necesarias para leer la instancia de la cual se buscaría su mejor solución, las semillas con la cuales se inicializarían los generadores de números aleatorios, y la información con la cual se determina el costo de las ciudades -tales como la latitud, longitud, y los caminos entre algunas de ellas-; podemos dividir la implementación de la solución en cinco componentes. En la mayoría de éstos, se buscó usar Programación Orientada a Objetos de la manera en que es posible usando Rust: usando un módulo con estructuras, y funciones dentro de un bloque de implementación. Esto en la práctica funciona muy parecido a la POO.\cite{Rustbook}\\

Los cinco componentes son: 

\begin{itemize}
    \item \textbf{Modelos}: Aquí, nos referimos a las estructuras \texttt{Ciudad} y \texttt{Conexion}, con los cuales se modelan los datos extraídos de la base de datos -válgame la redundancia-, y se utilizan para extraer las características de cada ciudad o conexión.
    \item \textbf{Gráfica}: Se creó un módulo Gráfica, que almacena la información de las ciudades y de sus conexiones entre ellas; y éstas a su vez generan los que llamamos la \textbf{gráfica original}. Puede determinar, usando una matriz de adyacencias, si dos ciudades están conectadas en la gráfica. Permite manejar la información extraída de la base de datos, y se auxilia de los modelos.
    \item \textbf{Solución}: Módulo que representa una posible solución al problema, utlizando una lista de identificadores de ciudades; donde cada par contiguo de ciudades representa una arista propuesta en la solución. Puede calcular el valor devuelto por la función de costo evaluada para esa misma solución, con apoyo de los datos obtenidos en la gráfica original; así como es capaz de obtener un nuevo vecino de la solución.
    \item \textbf{Temperatura Inicial}: Calcula la temperatura de "fusión" necesaria para que en la ejecución de la heurística, sean contempladas un cierto porcentaje alto de soluciones a partir de una configuración inicial; con el fin de que no sea tan alta como para que el sistema tarde mucho en terminar, pero sí lo suficiente para cubrir gran parte del espacio de búsqueda. 
    \item \textbf{Heurística}: Módulo en el que están contenidos todos los parámetros necesarios para ejecutar la heurística; y de los cuales depende la rapidez y calidad de los resultados. En el conjunto de métodos, se encuentran el encargado de realizar un lote -conjunto de soluciones-, y el método encargado de realizar la heurística.
\end{itemize}

\noindent
También se implementaron pruebas unitarias para lo referente a la extracción de datos de la base de datos, la consistencia en la información extraída, y el cálculo de funciones de costo para las soluciones candidatas del problema.\\

La implementación de la heurística se basa en el cálculo de lotes -o conjunto de soluciones vecinas-. Por cada lote calculado, se recolecta el promedio de los costos de las soluciones, y la mejor solución dentro del lote. Dentro del método que emula la heurística, mientras la temperatura sea mayor a un valor demasiado pequeño (y definido por nosotros mismos) y el promedio sea menor al equilibrio, guardamos el promedio del lote en una variable, y si la mejor solución del lote es mejor que la almacenada durante la ejecución de la heurística, almacenamos ésta. La temperatura decrementa si el promedio de un lote es mayor que el equilibrio.\\

Existen diversas variables que juegan un papel importante en la implementación de la heurística, cuyos valores óptimos pueden ser determinados mediante la experimentación.

\begin{itemize}
    \item \textbf{Tamaño del lote}: Cantidad de soluciones vecinas.
    \item \textbf{Máximo número de intentos por llenar el lote}: Es la cantidad de veces que se calculará una solución vecina en busca de llenar el lote. Es considerablemente mayor al tamaño preestablecido del lote.
    \item \textbf{Epsilon}: Es la temperatura a la cual se debe llegar para terminar la ejecución de la heurística.
    \item \textbf{Factor de enfriamiento}: Cantidad menor a 1 por la cual se multiplicará la temperatura para que decaiga y la heurística llegue a un fin.
    \item \textbf{Generador de números aleatorios}: Aunque no es tan cercano a la heurística, nos permite reproducir resultados; y dado que es utilizado en la creación de vecinos, también juega un papel importante en la obtención de resultados. Las mejores semillas son determinadas, además, por experimentación con gran cantidad de ellas.
\end{itemize}

\subsection*{Representación de las configuraciones}

\noindent
La configuración inicial, y las derivadas de ésta misma, están en forma de una lista de enteros, donde cada uno es el identificador de una ciudad previamente cargada en la base de datos. Podemos conocer la gráfica inducida por esta afirmación:

\vspace{5mm}

\begin{tcolorbox}[colback=lightmauve]
Gráfica inducida por configuración: $G = (V,E)$, donde los \textbf{vértices} son los identificadores en la configuración, y cada par de identificadores adyacentes en ésta corresponde a una \textbf{arista} de la gráfica.
\end{tcolorbox}

\vspace{5mm}

\noindent
Como las configuraciones vecinas se generan aleatoriamente, la gráfica inducida por éstas puede contener aristas que no estén presentes en la gráfica original. Para evitar considerar una de éstas como solución óptima, la función de costo de este tipo de configuraciónes será excesivamente alta.\\

Por el otro lado, las configuraciones donde cada arista de su gráfica está presente en la gráfica original, serán conocidas como \textbf{soluciones factibles}, y su costo será menor a uno.\footnote{Evito entrar en detalles específicos de la implementación, ya que los detalles están contenidos en \cite{Canekbook}}

\section*{Experimentación}

\subsection*{Parámetros de la heurística}

\noindent
Los parámetros que se encontraron óptimos para encontrar soluciones cercanas a la óptima se encuentran aquí:\\

\noindent
Para la generación de la temperatura inicial óptima:
\begin{itemize}
    \item \textbf{Temperatura inicial:} 32.
    \item \textbf{Porcentaje de soluciones aceptadas:} 80\% (o 0.8).
    \item \textbf{Epsilon - diferencia permitida de soluciones aceptadas con la fijada previamente}: 0.01.
\end{itemize}

\noindent
Para la heurística:
\begin{itemize}
    \item \textbf{Tamaño del lote}: $(length * (length-1)) / 2$, donde \textit{length} es la longitud de la instancia.
    \item \textbf{Número máximo de intentos}: $length^2$
    \item \textbf{Épsilon}: 0.0005
    \item \textbf{Factor de enfriamiento}: 0.95
\end{itemize}

\subsection*{Instancia 1: 40 ciudades}

\noindent
La primera instancia con la que se trabajó se compone de 40 ciudades, repartidas en Europa y Asía: 

\vspace{5mm}

\begin{tcolorbox}[title=Instancia de 40 ciudades]
1,2,3,4,5,6,7,163,164,165,168,172,327,329,331,332,333,489,490,491,492,493,496,653,654,656,
657,661,815,816,817,820,823,871,978,979,980,981,982,984
\end{tcolorbox}

\vspace{5mm}

\begin{figure}[H]
\centering
\includegraphics[scale=0.2]{instancia1_40.png}
\caption{Localización en el mapa de las 40 ciudades de las que se buscará la mejor ruta}
\end{figure}

\noindent
Se presenta la ejecución y resultados de tres de las mejores soluciones encontradas para esta instancia, dada la semilla con la que se inicializaron los generadores aleatorios, con el fin de poder reproducir los resultados:

\begin{tcolorbox}[colback=lightcarminepink!5!white, colframe=lightcarminepink!75!black,title=Semilla 755]
Costo: 0.224633
\tcblower
980,327,871,164,984,491,492,489,4,817,978,5,6,165,3,333,981,820,332,982,816,823,7,654,490,653,
656,2,661,657,168,1,815,496,172,163,329,493,979,331
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_755.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 40 ciudades con la semilla 755}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{solucion1_40.png}
\caption{Localización en el mapa de la mejor ruta para recorrer las 40 ciudades, calculada con la semilla 755}
\end{figure}

\vspace{1cm}

\begin{tcolorbox}[colback=lightcarminepink!5!white, colframe=lightcarminepink!75!black,title=Semilla 449]
Costo: 0.227558
\tcblower
661,657,168,1,815,496,172,2,656,653,7,823,816,982,332,820,654,490,163,329,493,979,4,165,3,333,
981,6,5,978,817,489,492,491,984,164,331,871,327,980
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_449.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 40 ciudades con la semilla 449}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{solucion2_40.png}
\caption{Localización en el mapa de la segunda mejor ruta para recorrer las 40 ciudades, calculada con la semilla 449}
\end{figure}

\vspace{1cm}

\begin{tcolorbox}[colback=lightcarminepink!5!white, colframe=lightcarminepink!75!black,title=Semilla 228]
Costo: 0.230544
\tcblower
978,5,6,817,4,165,3,333,981,820,332,982,816,823,7,654,490,653,656,2,661,657,168,1,815,496,172,
163,329,493,979,489,492,491,984,164,331,871,327,980
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_228.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 40 ciudades con la semilla 228}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.2]{solucion3_40.png}
\caption{Localización en el mapa de la tercer mejor ruta para recorrer las 40 ciudades, calculada con la semilla 228}
\end{figure}

\newpage

\subsection*{Instancia 2: 150 ciudades}

La segunda instancia con la que se trabajó consta de 150 ciudades alrededor del mundo:

\begin{tcolorbox}[title=Instancia de 150 ciudades]
1,2,3,4,5,6,7,8,9,11,12,14,16,17,19,20,22,23,25,26,27,74,75,151,163,164,165,166,167,168,169,171,
172,173,174,176,179,181,182,183,184,185,186,187,297,326,327,328,329,330,331,332,333,334,336,
339,340,343,344,345,346,347,349,350,351,352,353,444,483,489,490,491,492,493,494,495,496,499,
500,501,502,504,505,507,508,509,510,511,512,520,652,653,654,655,656,657,658,660,661,662,663,
665,666,667,668,670,671,673,674,675,676,678,815,816,817,818,819,820,821,822,823,825,826,828,
829,832,837,839,840,871,978,979,980,981,982,984,985,986,988,990,991,995,999,1001,1003,1004,
1037,1038,1073,1075
\end{tcolorbox}

\vspace{5mm}

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{instancia2_150.png}
\caption{Localización en el mapa de 100 de las 150 ciudades de las que se buscará la mejor ruta}
\end{figure}

\noindent
Se mostrarán a continuación tres de las mejores rutas encontradas para esta instancia, junto con su representación en un mapa, y un esquema de la ejecución:

\vspace{1cm}

\begin{tcolorbox}[colback=airforceblue!5!white, colframe=airforceblue!75!black,title=Semilla 3]
Costo: 0.166028
\tcblower
978,6,5,991,332,345,181,14,982,187,816,678,823,7,654,820,353,990,27,333,3,165,988,176,668,23,
352,817,347,499,501,11,164,444,826,340,183,512,821,75,346,171,652,1075,483,179,671,16,520,
675,12,1038,828,151,502,186,339,17,1003,349,331,662,8,837,979,493,509,329,19,168,505,986,
508,657,663,661,832,829,1,9,182,839,172,496,815,184,667,656,2,173,673,163,351,1004,185,22,
676,665,490,507,653,344,26,981,4,174,489,25,492,491,984,995,999,334,674,871,343,660,510,20,
500,825,985,504,670,511,327,350,840,336,297,980,1001,74,822,166,658,666,818,655,819,330,1073,
1037,169,326,328,167,495,494
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_3.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 150 ciudades con la semilla 3}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{solucion1_150.png}
\caption{Localización en el mapa de la mejor ruta para recorrer las 150 ciudades, calculada con la semilla 3}
\end{figure}

\vspace{1cm}

\begin{tcolorbox}[colback=airforceblue!5!white, colframe=airforceblue!75!black,title=Semilla 433]
Costo: 0.168154
\tcblower
829,1,986,508,661,832,663,657,9,168,665,676,185,22,351,978,352,817,347,499,491,25,837,979,493,
509,329,182,839,496,19,505,656,507,7,823,678,816,187,982,181,14,332,345,820,26,981,333,27,990,
353,991,490,654,653,344,673,2,173,184,667,815,172,163,1004,176,23,668,6,5,988,3,165,174,4,489,
492,501,164,11,1003,349,984,995,331,662,8,999,334,674,871,343,985,500,825,20,660,510,504,297,
74,980,336,840,350,670,327,511,826,444,17,75,183,346,171,652,1075,483,512,821,179,671,16,520,
186,339,675,1038,12,340,502,828,151,822,1001,166,658,666,818,655,819,330,1073,1037,169,326,
328,495,167,494
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_433.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 150 ciudades con la semilla 433}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{solucion2_150.png}
\caption{Localización en el mapa de la mejor ruta para recorrer las 150 ciudades, calculada con la semilla 433}
\end{figure}

\vspace{1cm}

\begin{tcolorbox}[colback=airforceblue!5!white, colframe=airforceblue!75!black,title=Semilla 329]
Costo: 0.172286
\tcblower
179,821,512,1075,652,483,171,346,75,183,16,671,520,675,186,339,826,840,350,511,327,670,500,20,
660,510,343,999,662,837,176,23,978,352,668,6,174,4,817,5,1004,351,22,185,991,27,353,990,333,
3,988,489,491,984,995,349,8,331,1003,334,504,985,825,871,674,11,164,501,492,499,347,979,493,
509,329,163,172,839,496,182,673,19,505,168,9,1,829,832,661,657,663,986,508,667,815,184,173,2,
656,507,665,654,490,344,653,7,823,678,816,187,181,982,14,345,332,26,820,676,981,165,25,17,444,
336,297,980,1038,12,828,340,502,151,822,1001,74,166,658,666,818,655,819,330,1073,169,1037,326,
328,495,167,494
\end{tcolorbox}

\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{ejec_329.png}
\caption{Costo de las soluciones encontradas a lo largo de la ejecución de la heurística para 150 ciudades con la semilla 329}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[scale=0.24]{solucion3_150.png}
\caption{Localización en el mapa de la mejor ruta para recorrer las 150 ciudades, calculada con la semilla 329}
\end{figure}

\begin{thebibliography}{9}
\bibitem{Kirkpatrick}
Kirkpatrick, S. (1984). Optimization by Simulated Annealing: Quantitative Studies. \textit{Journal of Statistical Physics}. 34(5/6). 975-986.

\bibitem{Dueck}
Dueck, G. \& Scheuer, T. (1990). Threshold Accepting: A General Purpose Optimization Algorithm Appearing Superior to Simulated Annealing. \textit{Journal of Computational Physics}. 90. 161-175.

\bibitem{Canekbook}
Canek Peláez Valdés. \textit{El Problema del Agente Viajero y Aceptación por Umbrales}. Texto auxiliar para el seminario de Heurísticas y Optimización Combinatoria.

\bibitem{Rustbook}
Klabnik, Steve, \& Nichols, Carol. (2019). Object Oriented Programming Features of Rust. \textit{The Rust Programming Language}. No Starch Press. Encontrado en \url{https://doc.rust-lang.org/book/ch17-00-oop.html}.

\bibitem{Repository}
Páes Alcalá, Alma Rosa. \textit{HOC\_PrimerProyecto}. Disponible en GitLab: \url{https://gitlab.com/AlmaPaes/hoc\_primerproyecto}.
\end{thebibliography}


\end{document}
