extern crate lazy_static;

#[cfg(test)]
mod tests {

    use crate::conexion_db::dbconection::get_data;
    use crate::proyecto::grafica::*;
    use crate::proyecto::solucion::*;
    use crate::proyecto::models::Ciudad;
    use crate::proyecto::models::Conexion;
    use lazy_static::lazy_static;

    lazy_static! {
        static ref DATOS: (Vec<Ciudad>, Vec<Conexion>) = get_data(); 
        static ref MI_GRAFICA: Grafica = Grafica::new(DATOS.0.to_vec(),DATOS.1.to_vec());
        static ref INSTANCIA1: Vec<i32> = vec![1,2,3,4,5,6,7,163,164,165,168,172,327,329,331,332,333,489,490,491,492,493,496,653,654,656,657,661,815,816,817,820,823,871,978,979,980,981,982,984];
        static ref MI_SOLUCION_1: Solucion<'static> = Solucion::new(INSTANCIA1.to_vec(), &MI_GRAFICA, 0f64, 0f64, 0f64);
        static ref INSTANCIA2: Vec<i32> = vec![1,2,3,4,5,6,7,8,9,11,12,14,16,17,19,20,22,23,25,26,27,74,75,151,163,164,165,166,167,168,169,171,172,173,174,176,179,181,182,183,184,185,186,187,297,326,327,328,329,330,331,332,333,334,336,339,340,343,344,345,346,347,349,350,351,352,353,444,483,489,490,491,492,493,494,495,496,499,500,501,502,504,505,507,508,509,510,511,512,520,652,653,654,655,656,657,658,660,661,662,663,665,666,667,668,670,671,673,674,675,676,678,815,816,817,818,819,820,821,822,823,825,826,828,829,832,837,839,840,871,978,979,980,981,982,984,985,986,988,990,991,995,999,1001,1003,1004,1037,1038,1073,1075];
        static ref MI_SOLUCION_2: Solucion<'static> = Solucion::new(INSTANCIA2.to_vec(), &MI_GRAFICA, 0f64, 0f64, 0f64);
    }

    #[test]
    fn test_get_distancia() {
        assert_eq!(MI_GRAFICA.get_distancia(1,7),2999396.2299999999813);
        assert_eq!(MI_GRAFICA.get_distancia(138,603),1132890.2800000000279);
        assert_eq!(MI_GRAFICA.get_distancia(863,968),MI_GRAFICA.get_distancia(968,863));
    }

    #[test]
    fn test_get_coordenadas(){
        //Compara coordenadas devueltas con las sacadas manualmente
        assert_eq!(MI_GRAFICA.get_coordenadas_ciudad(47),(17.3752999999999993,78.47440000000000282));
        assert_eq!(MI_GRAFICA.get_coordenadas_ciudad(159),(27.18329999999999913,78.016700000000000158));
        assert_eq!(MI_GRAFICA.get_coordenadas_ciudad(364),(36.616700000000001579,101.7669999999999959));
    }

    fn trunc_decimals(numero: f64, decimals: i32) -> f64{
        let factor = 10f64.powi(decimals);
        f64::trunc(numero * factor) / factor
    }

    #[test]
    fn test_distancia_natural(){
        //las aristas existen, así que la distancia natural debe ser la misma que la presente en la gráfica original
        assert_eq!(trunc_decimals(MI_SOLUCION_1.distancia_natural(834,1039),1),trunc_decimals(MI_GRAFICA.get_distancia(834,1039),1));
        assert_eq!(trunc_decimals(MI_SOLUCION_1.distancia_natural(127,130),1),trunc_decimals(MI_GRAFICA.get_distancia(127,130),1));
        
        assert!(MI_SOLUCION_1.distancia_natural(527,811) > 0f64);        
    }

   #[test]
   fn test_maxima_distancia(){
       assert_eq!(MI_SOLUCION_1.maxima_distancia, 4947749.060000000);
       assert_eq!(MI_SOLUCION_2.maxima_distancia, 4978506.480000000);
   }

   #[test]
   fn test_funcion_peso(){
       assert_eq!(MI_SOLUCION_1.funcion_peso(834,1039),MI_GRAFICA.get_distancia(834,1039));
       assert!(MI_SOLUCION_1.funcion_peso(527,811) > MI_SOLUCION_1.maxima_distancia);
   }

   #[test]
   fn test_normalizador(){
       assert_eq!(MI_SOLUCION_1.normalizador, 180836110.430000007);
       assert_eq!(MI_SOLUCION_2.normalizador, 723059620.720000267);
   }

   #[test]
   fn test_funcion_costo(){
       let factible1 = vec![29,88,152,319,31,2];
       let sol_temp_1 = Solucion::new(factible1, &MI_GRAFICA, 0f64, 0f64, 0f64);  
       assert!(sol_temp_1.funcion_de_costo() < 1f64); 

       let no_factible = vec![29,88,152,319,32,2];
       let sol_temp_2 = Solucion::new(no_factible, &MI_GRAFICA, 0f64, 0f64, 0f64);  
       assert!(sol_temp_2.funcion_de_costo() > 1f64); 
   }



}