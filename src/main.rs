#[macro_use]
extern crate diesel;
extern crate dotenv;

extern crate lazy_static;

mod proyecto;
mod conexion_db;
mod tests;

use crate::conexion_db::dbconection::get_data;
use crate::proyecto::grafica::*;
use crate::proyecto::solucion::*;
use crate::proyecto::heuristica::*;
use crate::proyecto::temperatura::*;
use std::time::{Instant};
use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use std::io;
use std::process;
use std::env;
use std::fs;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;


/// Función auxiliar para obtener la instancia a partir de la cadena de entrada
/// 
/// # Arguments
/// * `input` - la cadena con la entrada
/// 
/// # Returns
/// * `Vec<i32>` - el vector con las ciudades de la instancia
fn get_instancia(input: String) -> Vec<i32>{
    let instancia;
    if fs::metadata(&input).is_ok(){
        instancia = fs::read_to_string(input)
        .expect("Something went wrong reading the file");
    } else{
        instancia = input;
    }

    let x: &[_] = &['\r', '\n'];
    let cadena = instancia.trim_end_matches(x);
    let vector: Vec<&str> = cadena.split(',').collect();
    let nums: Vec<i32> = vector.into_iter().map(|x| x.parse().unwrap()).collect();    
    nums
}


/// Función auxiliar para obtener una permutación aleatoria de una instancia
/// 
/// # Arguments
/// * `instancia` - Vector del cual obtendremos una permutación aleatoria
/// * `semilla` - Semilla para inicializar el generador de números aleatorios, y para permitirnos reproducir los resultados
/// 
/// # Returns
/// * `Vec<i32>` - una permutación de la instancia recibida
/// 
fn permutacion_instancia(instancia: &Vec<i32>, semilla: u64) -> Vec<i32>{
    let mut rng =  <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);
    let mut nueva_instancia = instancia.to_vec();
    let i = nueva_instancia.as_mut_slice();
    i.shuffle(&mut rng);
    i.to_vec()
}


/// Obtiene las semillas para los generadores de números aleatorios a partir de los argumentos
/// 
/// # Arguments
/// * `args` - argumentos pasados en la llamada a ejecución de main
/// 
/// # Returns
/// * `Result<Vec<u64> &'static str>` - tipo Result, ya sea con el vector de semillas (u64), o con un mensaje de error
fn get_argumentos(args: Vec<String>) -> Result<Vec<u64>, &'static str>{
    if args.len() < 2{
        return Err("Argumentos incorrectos: {semillas}");
    }

    let mut semillas: Vec<u64> = Vec::new();
    let argumentos = args.as_slice();
    for a in argumentos.split_first().unwrap().1.iter(){
        match a.parse::<i32>(){
            Ok(seed) => semillas.push(seed as u64),
            Err(_) => {
                    println!("Semilla incorrecta. Sólo son aceptados números enteros");
                    process::exit(1);
            }
        }
    }
    
    Ok(semillas)

}


/// Ejecución de la heurística
/// 
/// # Arguments
/// * `semilla` - semilla con la cual se inicializará el generador de números aleatorios
/// * `grafica` - representación de la gráfica del problema
/// * `instancia` - instancia de la cual encontraremos la solución óptima
/// 
/// # Returns
/// * `Vec<i32>` - permutación de la instancia original que representa la mejor solución obtenida
fn run_heuristica(semilla: u64, grafica: &Grafica, instancia: &Vec<i32>) -> Vec<i32>{
    //Creamos el generador
    let mut rng =  <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);

    //Creamos la gráfica original y la solución con la instancia
    let solucion = Solucion::new(instancia.to_vec(), &grafica, 0f64, 0f64, 0f64);

    //Calculamos la temperatura inicial
    let temp_inicial = temperatura_inicial(solucion.get_copy(),32f64,0.8, &mut rng);

    let sol_length = solucion.get_length() as i32;
    let mut heuristica = Heuristica::new(sol_length * (sol_length-1)/2, 
                                        sol_length * sol_length,
                                    0.0005f64, 0.95f64, semilla);

    //Encontramos la mejor solución
    let aceptada = heuristica.aceptacion_umbrales(temp_inicial, solucion);

    let costo = aceptada.funcion_de_costo();

    let mut solucion_aceptada = aceptada.ciudades_problema;
    let ultimo = solucion_aceptada.pop();

    for item in solucion_aceptada.iter(){
        print!("{},",item);
    }
    println!("{}",ultimo.unwrap());

    println!("Costo: {}", costo);

    solucion_aceptada
}

/// # Función principal
fn main() {
    let args: Vec<String> = env::args().collect();
    let semillas: Vec<u64>;
    match get_argumentos(args){
        Ok(datos) => semillas = datos,
        Err(e) => {
            println!("{}",e);
            process::exit(1);
        }
    }

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("No se pudo leer la entrada");
    let instancia = Arc::new(Mutex::new(get_instancia(input)));

    let (ciudades,conexiones) = get_data();
    let grafica_original = Arc::new(Mutex::new(Grafica::new(ciudades,conexiones)));
    let resultados: std::sync::Arc<std::sync::Mutex<std::vec::Vec<Vec<i32>>>> = Arc::new(Mutex::new(Vec::new()));

    //let now = Instant::now();
    let pool = ThreadPool::new(10);

    for seed in semillas.into_iter(){
        let mi_grafica = Arc::clone(&grafica_original);
        let mi_instancia = Arc::clone(&instancia);
        let resultados = Arc::clone(&resultados);
        pool.execute(move || {
            &resultados.lock().unwrap().push(run_heuristica(seed, &*mi_grafica.lock().unwrap(), &permutacion_instancia(&*mi_instancia.lock().unwrap(),seed)));
        })
    }
    pool.join();
    //let new_now = Instant::now();
    //println!("{:?}", new_now.duration_since(now));    
}