use crate::proyecto::solucion::*;
use rand_chacha::ChaCha8Rng;
use rand::prelude::*;

/// Modela los parámetros necesarios para mdoelar una ejecución de la heurística de recocido simulado
pub struct Heuristica{
    /// Cantidad de soluciones aceptadas en un lote
    exitos_lote: i32,
    /// Cantidad máxima de vecinos que pueden generarse en búsqueda de llenar el lote
    max_intentos_lote: i32,
    /// Temperatura a la cual se debe llegar para detener el recocido
    epsilon_umbrales: f64,
    /// Factor de enfriamiento, con el cual se decrementará la temperatura
    factor_frio: f64,
    /// Generador de números aleatorios para generar nuevos vecinos
    rnd_generator: rand_chacha::ChaCha8Rng,
}

impl<'a> Heuristica{
    /// Crea una nueva simulación del recocido - nueva Heuristica
    /// 
    /// # Arguments
    /// * `exitos_lote` - capacidad de los lotes
    /// * `max_intentos_lote - número máximo de vecinos encontrados para generar el lote
    /// * `epsilon_umbrales` - temperatura a la cual se debe llegar para detener el recocido
    /// * `factor_frio` - factor de enfriamiento
    /// * `semilla` - semilla con la cual se inicializará el generador de números aleatorios para conseguir vecinos
    pub fn new(exitos_lote: i32, max_intentos_lote: i32, epsilon_umbrales: f64, 
        factor_frio: f64, semilla: u64) -> Heuristica{
            //Creamos el generador
            let rng =  <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);
            Heuristica{
                exitos_lote,
                max_intentos_lote,
                epsilon_umbrales,
                factor_frio,
                rnd_generator: rng
            }
    }

    /// Calcula un lote dada una solución y temperatura iniciales, y devuelve el promedio de las soluciones aceptadas, y la mejor
    /// 
    /// # Arguments
    /// * `solucion inicial` - solución a partir de la cual vamos a sacar un lote
    /// * `temp_inicial` - temperatura inicial
    /// 
    /// # Returns
    /// * `f64` - promedio de las soluciones aceptadas 
    /// * `Solucion` - última solución aceptada
    pub fn calcula_lote(&mut self, solucion_inicial: Solucion<'a>, temp_inicial: f64) -> (f64,Solucion<'a>){
        let mut counter_exitos = 0;
        let mut intentos = 0;
        let mut sum_results = 0.0f64;
        let mut solucion_actual; 
        let mut solucion_aceptada: Solucion<'a> = solucion_inicial.get_copy();
        let mut costo_actual;
        let mut sol_counter = 0;

        while counter_exitos <= self.exitos_lote && intentos <= self.max_intentos_lote {
            solucion_actual = solucion_aceptada.get_vecino(&mut self.rnd_generator);
            costo_actual = solucion_actual.funcion_de_costo();
            if costo_actual <= solucion_aceptada.funcion_de_costo() + temp_inicial{
                if sol_counter % 20 == 0{
                    print!("{:.7},",costo_actual);
                }
                sol_counter += 1;
                sum_results += costo_actual;
                solucion_aceptada = solucion_actual;
                counter_exitos += 1;
            }
            else{
                intentos += 1;
            }
            
        }
        (sum_results / (self.exitos_lote as f64), solucion_aceptada)
    }

    /// Método de aceptación por umbrales
    /// 
    /// # Arguments
    /// * `temperatura` - temperatura inicial 
    /// * `solucion_inicial` - solución inicial
    /// 
    /// # Returns
    /// * `Solucion` - mejor solución encontrada
    pub fn aceptacion_umbrales(&mut self, temperatura: f64, solucion_inicial: Solucion<'a>) -> Solucion<'a>{
        let mut promedio = 0.0f64; //p
        let mut equilibrio; //q  
        let mut temper = temperatura;
        let mut lote;
        let mut mejor_solucion: Solucion<'a> = solucion_inicial.get_copy();
        let mut solucion_actual: Solucion<'a>;
        let mut lotes;

        while temper > self.epsilon_umbrales{
            equilibrio = f64::INFINITY;
            lotes = 0;
            while promedio <= equilibrio {
                equilibrio = promedio; 
                lote = self.calcula_lote(mejor_solucion.get_copy(), temper);
                lotes += 1;
                solucion_actual = lote.1;
                promedio = lote.0;
                
                if solucion_actual.funcion_de_costo() < mejor_solucion.funcion_de_costo(){
                    mejor_solucion = solucion_actual;
                }
                if lotes == 100{ break; }
            }
            temper = self.factor_frio * temper;
        }

        mejor_solucion
    }
}