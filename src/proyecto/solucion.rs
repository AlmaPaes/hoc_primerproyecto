use crate::proyecto::grafica::*;
use combinations::Combinations;
use rand::prelude::*;


/// Modela una solución al problema de TSP
pub struct Solucion<'a>{
    /// Ciudades de la instancia a considerar. 
    /// Su orden determina las aristas consideradas en la solución.
    pub ciudades_problema: Vec<i32>,
    /// Gráfica con todas las ciudades y conexiones posibles.
    pub grafica_original: &'a Grafica,
    /// La distancia más grande entre las ciudades de la solución, cuya conexión esté presente en la gráfica
    pub maxima_distancia: f64,
    /// Suma de las n-1 distancias más grandes, para obtener costos menores a 1
    pub normalizador: f64,
    /// Suma de los pesos de sus aristas, para sacar el costo de la solución
    pub suma_pesos: f64,
}

/// Función para obtener todas las combinaciones de dos ciudades.
/// En este caso, (a,b) = (b,a)
/// 
/// # Arguments
/// * `ciudades` - Vector de id's de ciudades, de las cuales extraeremos sus combinaciones
/// 
/// # Returns 
/// * `Vec<Vec<i32>>` - Vector de vectores, donde cada vector es una combinación
fn get_comb<'a>(ciudades: &Vec<i32>) -> Vec<Vec<i32>>{
    Combinations::new((ciudades).to_vec(), 2).collect()
}

/// Obtiene la distancia máxima entre todas las ciudades que estén conectadas en la gráfica original
/// 
/// # Arguments
/// * `ciudades` - vector de ciudades entre las cuales vamos a considerar sus conexiones
/// * `grafica` - gráfica en la que buscaremos las conexiones entre las ciudades
/// 
/// # Returns 
/// * `f64` - la distancia máxima entre ciertas dos ciudades en la gráfica
pub fn distancia_maxima<'a>(ciudades: &Vec<i32>, grafica: &'a Grafica) -> (f64,f64) {
        let mut lista: Vec<f64> = Vec::new();
        let mut actual = 0.0f64;
        let cities = get_comb(ciudades);
        cities.into_iter().for_each(|it| {
            actual = grafica.get_distancia(it[0],it[1]);
            if actual > 0.0f64 {
                lista.push(actual);
            }
        });
        lista.sort_by(|a, b| b.partial_cmp(a).unwrap());
        if lista.len() > ciudades.len()-1 {
            lista.resize(ciudades.len()-1,0.0f64);
        }

        let primero = lista.as_slice().first();

        let mut suma = 0.0f64;
        for cantidad in lista.iter(){
            suma += cantidad;
        }
        (suma, *primero.unwrap())
        
}

impl<'a> Solucion<'a>{

    /// Crea una nueva solución
    /// 
    /// # Arguments
    /// * `ciudades` - ciudades consideradas en la solución
    /// * `grafica` - gráfica en la cual nos vamos a basar para conocer las conexiones entre las ciudades
    /// * `rnd` - generador de nùmeros aleatorio, para crear vecinos de esta solución
    pub fn new(ciudades: Vec<i32>, grafica: &'a Grafica, suma_pesos: f64, normalizador: f64, maximo: f64) -> Solucion<'a>{
        let datos = 
        if normalizador > 0f64 && maximo > 0f64{
            (normalizador,maximo)
        } else{
            distancia_maxima(&ciudades,grafica)
        };
        let (norm,maxim) = datos;
        Solucion{
            ciudades_problema: ciudades,
            grafica_original: grafica,
            maxima_distancia: maxim,
            normalizador: norm,
            suma_pesos: suma_pesos,
        }

    }

    /// Devuelve la cantidad de ciudades considerada en esta solución
    /// 
    /// # Returns
    /// * `i32` - cantidad de ciudades
    pub fn get_length(&self) -> i32{
        self.ciudades_problema.len() as i32
    }

    /// Devuelve una copia de esta misma solución
    /// 
    /// # Returns 
    /// * `Solución` - copia de la solución actual
    pub fn get_copy(&self) -> Solucion<'a>{
        Solucion::new(self.ciudades_problema.as_slice().to_vec(), self.grafica_original, self.suma_pesos, self.normalizador, self.maxima_distancia)
    }

    /// Para saber si calcular por primera vez la suma de sus aristas, o si el peso está disponible
    #[doc(hidden)]
    pub fn get_suma_valores(&self) -> f64{
        if self.suma_pesos == 0f64{
            self.suma_pesos()
        }
        else{
            self.suma_pesos
        }
    }
    
    /// Convierte grados a radianes
    #[doc(hidden)]
    fn grad_to_rad(grado: f64) -> f64{
        (grado * std::f64::consts::PI) / 180f64
    }

    /// Para obtener el valor A, usado para calcular la distancia natural
    #[doc(hidden)]
    fn get_a_value(par1: (f64,f64), par2: (f64,f64)) -> f64{
        let (lat1,long1) = &par1;
        let (lat2,long2) = &par2;
        let mut term1 = (lat2 - lat1) / 2f64;
        term1 = (&mut term1.sin()).powi(2);
        let mut term2 = (long2 - long1) / 2f64;
        term2 = (&mut term2.sin()).powi(2) * lat1.cos() * lat2.cos();
        term1 + term2
    }

    /// Calcula y devuelve la distancia natural entre dos ciudades
    /// 
    /// # Arguments 
    /// * `id1` - id de la primera ciudad
    /// * `id2` - id de la segunda ciudad
    /// 
    /// # Returns 
    /// `f64`- distancia natural entre las dos ciudades
    pub fn distancia_natural(&self, id1: i32, id2: i32) -> f64{
        let radio = 6373000f64;
        let (lat1,long1) = self.grafica_original.get_coordenadas_ciudad(id1);
        let (lat2,long2) = self.grafica_original.get_coordenadas_ciudad(id2);

        let a_value = Solucion::get_a_value(
            (Solucion::grad_to_rad(lat1),Solucion::grad_to_rad(long1)),
            (Solucion::grad_to_rad(lat2),Solucion::grad_to_rad(long2)));

        let c_haversine = 2f64 * (a_value.sqrt()).atan2((1f64 - a_value).sqrt());
        radio * c_haversine
    }

    /// Devuelve el peso de una arista entre dos ciudades, de acuerdo a la función de peso aumentada
    /// 
    /// # Arguments 
    /// `id1` - id de la primera ciudad 
    /// `id2` - id de la segunda ciudad
    /// 
    /// # Returns 
    /// `f64` - peso de la arista entre las dos ciudades
    pub fn funcion_peso(&self, id1: i32, id2: i32) -> f64{
        let peso_e = self.grafica_original.get_distancia(id1,id2);
        let mut peso = peso_e; 
        if peso_e == 0f64{
            peso = self.distancia_natural(id1,id2) * self.maxima_distancia;
        }
        peso
    }

    /// Función para calcular por primera vez la suma de las aristas de la solución
    /// 
    /// # Returns
    /// * `f64` - la suma de los pesos de las aristas
    fn suma_pesos(&self) -> f64{
        let mut suma_pesos = 0.0f64;
        let arreglo = self.ciudades_problema.as_slice();
        for pedazo in arreglo.windows(2){
            suma_pesos += self.funcion_peso(pedazo[0],pedazo[1]);
        }
        suma_pesos
    }

    #[doc(hidden)]
    fn aux_get_peso(&self, sum: i32, indice1: usize, indice2: usize) -> f64{
        let arreglo = self.ciudades_problema.as_slice();
        let distancia = self.funcion_peso(arreglo[indice1], arreglo[indice2]);
        (sum as f64) * distancia
    }


    /// Función que actualiza la suma de pesos de las aristas
    /// 
    /// # Arguments
    /// * `id1` - id de la primera ciudad que cambió de lugar
    /// * `id2` - id de la segunda ciudad que cambió de lugar
    /// 
    /// # Returns
    /// * `f64` - suma de los pesos actualizada
    pub fn actualiza_sumapesos(&self, id1: i32, id2: i32) -> f64{
        let mut nueva_suma = self.get_suma_valores();
        let mayor: usize;
        let menor: usize = 
            if id1 < id2{
                mayor = id2 as usize;
                id1 as usize
            } else {
                mayor = id1 as usize;
                id2 as usize
            };
        
        //son vecinos
        if (mayor - menor) <=2{
            if menor == 0{
                nueva_suma += self.aux_get_peso(-1, mayor, mayor+1);
                nueva_suma += self.aux_get_peso(1, menor, mayor+1);
            }
            if mayor == self.ciudades_problema.len() - 1{
                nueva_suma += self.aux_get_peso(-1, menor, menor-1);
                nueva_suma += self.aux_get_peso(1, menor-1, mayor);
            }
            else{
                nueva_suma += self.aux_get_peso(-1, menor-1, menor);
                nueva_suma += self.aux_get_peso(1, menor-1, mayor);


                nueva_suma += self.aux_get_peso(-1, mayor, mayor+1);
                nueva_suma += self.aux_get_peso(1, menor, mayor+1);
            }
        }else { 
            if menor == 0{
                nueva_suma += self.aux_get_peso(-1, menor, menor+1);
                nueva_suma += self.aux_get_peso(1, menor, mayor-1);
                nueva_suma += self.aux_get_peso(1, menor, mayor+1);
                nueva_suma += self.aux_get_peso(-1, mayor-1, mayor);
                nueva_suma += self.aux_get_peso(-1, mayor, mayor+1);
                nueva_suma += self.aux_get_peso(1, menor+1, mayor);
            } 
            if mayor == self.ciudades_problema.len() - 1{
                nueva_suma += self.aux_get_peso(-1, menor-1, menor);
                nueva_suma += self.aux_get_peso(-1, menor, menor+1);
                nueva_suma += self.aux_get_peso(1, menor, mayor-1);

                nueva_suma += self.aux_get_peso(-1, mayor-1, mayor);
                nueva_suma += self.aux_get_peso(1, menor-1, mayor);
                nueva_suma += self.aux_get_peso(1, menor+1, mayor);
            }
            else{
                nueva_suma += self.aux_get_peso(-1, menor-1, menor);
                nueva_suma += self.aux_get_peso(-1, menor, menor+1);
                nueva_suma += self.aux_get_peso(1, menor, mayor-1);
                nueva_suma += self.aux_get_peso(1, menor, mayor+1);

                nueva_suma += self.aux_get_peso(-1, mayor-1, mayor);
                nueva_suma += self.aux_get_peso(-1, mayor, mayor+1);
                nueva_suma += self.aux_get_peso(1, menor-1, mayor);
                nueva_suma += self.aux_get_peso(1, menor+1, mayor);
                

            }
        }
        nueva_suma
    }


    /// Calcula el costo de la solución actual.
    /// Es la función que se buscará optimizar en la heurística.
    /// 
    /// # Returns
    /// * `f64` - el costo de la solución actual
    pub fn funcion_de_costo(&self) -> f64{
        self.get_suma_valores() / self.normalizador
    }

    /// Intercambia dos valores del vector de las ciudades
    #[doc(hidden)]
    fn swap_values(&self, index1: usize, index2: usize) -> Vec<i32>{
        let mut vec = self.ciudades_problema.to_vec();
        let temp = vec[index1];
        vec[index1] = vec[index2];
        vec[index2] = temp;
        vec
    }

    //Obtiene un índice aleatorio para acceder a una ciudad, sin repetir ciertos índices
    #[doc(hidden)]
    fn get_new_index(&self, rng: &mut rand_chacha::ChaCha8Rng, different1: i32, different2: i32) -> i32{
        let mut new_index;
        let index = loop{
            new_index = rng.gen_range(0, self.ciudades_problema.len() as i32);
            if new_index != different1 && new_index!=different2 {
                break new_index;
            }
        };

        index
        
    }

    /// Genera un nuevo vecino a partir de esta solución
    /// 
    /// # Arguments
    /// * `rng` - generador de números aleatorios con el cual se creará el nuevo vecino
    /// 
    /// # Returns   
    /// * `Solucion` - vecino de la solución actual
    pub fn get_vecino(&self, rng: &mut rand_chacha::ChaCha8Rng) -> Solucion<'a>{
        let index1 = self.get_new_index(rng, 0,-1);
        let index2 = self.get_new_index(rng, 0, index1);
        let nuevo_ejemplar = self.swap_values(index1 as usize, index2 as usize);
        let nueva_suma = self.actualiza_sumapesos(index1, index2);
        Solucion::new(nuevo_ejemplar,self.grafica_original, nueva_suma, self.normalizador, self.maxima_distancia)
    }

}