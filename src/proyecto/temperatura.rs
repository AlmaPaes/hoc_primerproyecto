use crate::proyecto::solucion::*;

/// Función para crear la temperatura inicial que aumente la probabilidad de que la heurística pueda desplazarse rápidamente por el espacio de búsqueda.
/// 
/// # Arguments
/// * `solucion` - solución a partir de la cual se va a calcular la temperatura inicial
/// * `temperatura` - temperatura inicial
/// * `porcentaje_soluciones`- porcentaje de soluciones aceptadas
/// * `rng` - generador de números aleatorios que se usará para generar vecinos
/// 
/// # Returns 
/// * `f64` - temperatura inicial para la heurística
pub fn temperatura_inicial(solucion: Solucion, temperatura: f64, porcentaje_soluciones: f64, rng: &mut rand_chacha::ChaCha8Rng) -> f64{
    let mut porc = porcentaje_aceptados(solucion.get_copy(), temperatura, rng);

    let epsilon = 0.01;
    let mut temp = temperatura;
    let temp1;
    let temp2;

    if (porcentaje_soluciones - porc) <= epsilon{
        return temp;
    }
    if porc < porcentaje_soluciones {
        while porc < porcentaje_soluciones {
            temp = 2f64 * temp;
            porc = porcentaje_aceptados(solucion.get_copy(), temp, rng)
        }

        temp1 = temp / 2f64;
        temp2 = temp;
    }

    else{
        while porc > porcentaje_soluciones {
            temp = temp / 2f64;
            porc = porcentaje_aceptados(solucion.get_copy(), temp, rng);
        }

        temp1 = temp;
        temp2 = 2f64 * temp;
    }

    return busqueda_binaria(solucion, temp1, temp2, porcentaje_soluciones, rng);
}

/// Calcula el porcentaje de soluciones aceptadas dada una cierta solución y temperatura
/// 
/// # Arguments
/// * `solucion` - solución a partir de la cual se crearán nuevas
/// * `temperatura` - temperatura con la que se calcularán las soluciones aceptadas
/// * `rng` - generador de números aleatorios que se usará para generar vecinos
/// 
/// # Returns
/// * `f64` - porcentaje de soluciones aceptadas, entre 0 y 1.
pub fn porcentaje_aceptados(solucion: Solucion, temperatura: f64, rng: &mut rand_chacha::ChaCha8Rng) -> f64{
    let mut counter = 0;
    let length_solution = solucion.get_length();
    let number = (length_solution * (length_solution-1)) / 2;
    let mut solucion_actual;
    let mut mejor_solucion = solucion;

    for _i in 1..=number {
        solucion_actual = mejor_solucion.get_vecino(rng);
        if solucion_actual.funcion_de_costo() <= mejor_solucion.funcion_de_costo() + temperatura{
            counter += 1;
            mejor_solucion = solucion_actual;
        }
    }

    (counter as f64) / (number as f64)
}

/// Realiza búsqueda binaria de la temperatura necesaria para obtener un porcentaje elevado de soluciones aceptadas
/// 
/// # Arguments
/// * `solucion` - solución a partir de la cual se va a calcular la temperatura inicial
/// * `temp1` - cota inferior del rango de temperaturas en la cual se hará la búsqueda
/// * `temp2` - cota superior del rango de temperaturas en la cual se hará la búsqueda
/// * `perc_soluciones`- porcentaje de soluciones aceptadas
/// * `rng` - generador de números aleatorios que se usará para generar vecinos
/// 
/// # Returns 
/// * `f64` - temperatura inicial 
pub fn busqueda_binaria(solucion: Solucion, temp1: f64, temp2: f64, perc_soluciones: f64, rng: &mut rand_chacha::ChaCha8Rng) -> f64{
    let mut t1 = temp1;
    let mut t2 = temp2;
    let epsilon_perc = 0.01;
    let mut porcentaje;

    while t1 < t2 {
        let temp_med = (t1+t2) / 2f64;

        if t2 - t1 < epsilon_perc{
            return temp_med;
        }

        porcentaje = porcentaje_aceptados(solucion.get_copy(), temp_med, rng);

        if (perc_soluciones - porcentaje).abs() < epsilon_perc{
            return temp_med;
        }


        if porcentaje > perc_soluciones {
            t2 = temp_med;
        } else {
            t1 = temp_med;
        }
    }

    return -1f64;
}
