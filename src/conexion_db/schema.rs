table! {
    cities {
        id -> Integer,
        name -> Text,
        country -> Text,
        population -> Integer,
        latitude -> Double,
        longitude -> Double,
    }
}

table! {
    connections (id_city_1, id_city_2) {
        id_city_1 -> Integer,
        id_city_2 -> Integer,
        distance -> Double,
    }
}