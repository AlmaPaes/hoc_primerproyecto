extern crate diesel;
extern crate dotenv;
//extern crate tsp_project;

use crate::proyecto::models::Ciudad;
use crate::proyecto::models::Conexion;

use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use diesel::connection::Connection;

/// Establece la conexión con la base de datos
/// 
/// # Returns
/// * `SqliteConnection` - conexión a la base de datos usando *sqlite*
pub fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

/// Extrae los datos almacenados en la base de datos, los guarda y devuelve
/// 
/// # Returns
/// * `(Vec<Ciudad>, Vec<Conexion>)` - tupla de vectores con las ciudades y sus conexiones
pub fn get_data() -> (Vec<Ciudad>, Vec<Conexion>){
    use crate::conexion_db::schema::cities::dsl::*;
    use crate::conexion_db::schema::connections::dsl::*;

    let connection = establish_connection();
    let results_cities = cities
        .load::<Ciudad>(&connection)
        .expect("Error loading cities");

    let results_connections = connections
        .load::<Conexion>(&connection)
        .expect("Error loading connections");


    (results_cities, results_connections)
}